public class Exercici2 implements Runnable
{
    long start;

    public Exercici2(long num)
    {
        start = num;
    }

    private boolean isPrime(long num)
    {
        for (int i = 2; i < num; i++)
        {
            if (num % i == 0)
            {
                System.out.println(Thread.currentThread().getName() + " " + num + " is prime.");
                return true;
            }
        }
        System.out.println(Thread.currentThread().getName() + " " + num + " is NOT prime.");
        return false;
    }

    @Override
    public void run()
    {
        long closestPrime = 0;
        long right = start + 1, left = start - 1;
        while (closestPrime == 0)
        {
            if (isPrime(left))
                closestPrime = left;
            else if (isPrime(right))
                closestPrime = right;
            else
            {
                right++;
                left--;
            }
        }
        System.out.println(Thread.currentThread().getName() + " The closest prime is: " + closestPrime);
    }
}
