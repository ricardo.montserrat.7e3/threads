public class Prova
{
    public static void main(String[] args)
    {
        Thread t1Exercici1 = new Thread(new Exercici1("small.txt"));
        Thread t2Exercici1 = new Thread(new Exercici1("medium.txt"));
        Thread t3Exercici1 = new Thread(new Exercici1("big.txt"));

        t1Exercici1.start();
        t2Exercici1.start();
        t3Exercici1.start();

        Thread t1Exercici2 = new Thread(new Exercici2(10));
        Thread t2Exercici2 = new Thread(new Exercici2(20));
        Thread t3Exercici2 = new Thread(new Exercici2(30));

        t1Exercici2.start();
        t2Exercici2.start();
        t3Exercici2.start();
    }
}
