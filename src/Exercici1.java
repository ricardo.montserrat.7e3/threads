import javax.imageio.stream.ImageInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Exercici1 implements Runnable
{
    private String file;

    public Exercici1(String f)
    {
        file = f;
    }

    private int countLines(String fil)
    {
        File f = new File(fil);
        int lines = 0;
        if (f.exists())
        {
            try
            {
                FileReader r = new FileReader(f);
                BufferedReader reader = new BufferedReader(r);
                while (reader.readLine() != null)
                {
                    lines++;
                    System.out.println(Thread.currentThread().getName() + " Line: " + lines);
                }

                r.close();
                reader.close();
            }
            catch (Exception e)
            {
                System.out.println(e.toString());
            }
        }
        else
            System.out.println("[FILE]" + f.getName() + " doesn't exist!");
        return lines;
    }

    @Override
    public void run()
    {
        System.out.println(Thread.currentThread().getName() + " has " + countLines(file) + " lines");
    }
}
